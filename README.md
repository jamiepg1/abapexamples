ABAP Examples
=============

_To view examples goto the wiki. Corresponding code is kept here._

&nbsp;&nbsp;&nbsp;&nbsp;As a programmer, and one who works with many languages, I've made extensive use of Google and other internet search engines. The internet serves as a great reference guide for problems which I forget minor details to looking up things which I've never even seen before.

&nbsp;&nbsp;&nbsp;&nbsp;One thing that a programming language can really benefit from is good documentation. Documentation can illustrate to developers what is avaliable to them to solve any issues they come up across. Good documentation even helps give insight to issues less tackled.

## ABAP is just a programming language. 

&nbsp;&nbsp;&nbsp;&nbsp;The nature of `ABAP` is a progamming language. Just like `C`, `C++`, `C#`, `PHP`, `Java`, `Javascript`, `Basic`, or whatever language, `ABAP` has classes, interfaces, methods, variables, and arithmetic. `ABAP` supports math and logic which we as developers take advantage of to encapsulate the notion of a physical reality within a computer program. The nature of your individual responsibilities for customers is business, and changes from each job or project.

## Clean, simple, technical examples

&nbsp;&nbsp;&nbsp;&nbsp;To keep things simple the examples will be illustrated with as least usage of standard SAP data content, such as tables or particular reports. No copy &amp; pasted code, no unformatted text. Simple ABAP programs that are clearly explained. 

## Contributing 

Want to add a tutorial? No problem! Just fork the repository, create a folder, commit the code!
